extends Node2D

# Code taken from the gdquest-tuorial reference:
# https://www.gdquest.com/tutorial/godot/audio/background-music-transition/

onready var _anim_player := $AnimationPlayer
onready var _track1 := $Track1
onready var _track2 := $Track2

func set_next_track(audio_stream : AudioStream) -> void:
	if(_track1.playing and _track2.playing):
		return
		
	if _track2.playing:
		_track1.stream = audio_stream
		_track1.play()
		_anim_player.play("FadeToTrack1");
	else:
		_track2.stream = audio_stream
		_track2.play()
		_anim_player.play("FadeToTrack2");





