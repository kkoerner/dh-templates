extends Node2D

signal soundchange(track)
signal finished

export (String) var id = "cs1"

export (Array, NodePath) var textWriterPositions
export (Array, String) var textsToShow
export (Array, int) var positions

export (Array, Texture) var backgroundImgs
export (Array, int) var backgroundImgMapping

export (Array, AudioStreamOGGVorbis) var backgroundSounds
export (Array, int) var backgroundSoundsMapping

var currentPosition = 0
var soundChanged = false
var finished = false

func _ready():
	$AnimationPlayer.play("InitalFadeIn")
	set_sound()

func _start():
	$InBetweenTimer.start()

func _process(_delta):
	if finished and Input.is_action_just_pressed("ui_accept"):
		change_scene()

func _on_TextWriter_text_displayed():
	currentPosition += 1;
	if(currentPosition < backgroundImgMapping.size() && backgroundImgMapping[currentPosition-1] != backgroundImgMapping[currentPosition]):
		$AnimationPlayer.play("FadeOut")
	else:
		$InBetweenTimer.start();
	

func _on_InBetweenTimer_timeout():
	if(currentPosition < textsToShow.size()):
		if(!soundChanged):
			set_sound()
		var position = positions[currentPosition]
		var textWriter = textWriterPositions[position]
		var last_text_in_scene = currentPosition == textsToShow.size() - 1
		get_node(textWriter).displayText(textsToShow[currentPosition], last_text_in_scene)
		soundChanged = false
	

func _show_proceeding_btn():
	emit_signal("finished")
	finished = true

func _start_on_InBetweenTimer():
	$InBetweenTimer.start()

func next_Background():
	$Background.texture = backgroundImgs[backgroundImgMapping[currentPosition]]
	$AnimationPlayer.play("FadeIn")
	set_sound()
	

func set_sound():
	if(currentPosition == 0 || (currentPosition < backgroundSoundsMapping.size() && backgroundSoundsMapping[currentPosition-1] != backgroundSoundsMapping[currentPosition])):
		
		var pos = backgroundSoundsMapping[currentPosition]
		var sound = backgroundSounds[pos]
		
		emit_signal("soundchange", sound);
		
		soundChanged = true

func change_scene():
	$MutexLoader/NextSceneLoader.perform_scene_change()

func _on_TextWriter_last_text_displayed():
	finished = true
