extends Label

signal text_displayed
signal last_text_displayed

var textToDisplay
var initial_modulate
var last_text = false

func _ready():
	initial_modulate = self.modulate

func _process(_delta):
	if($Timer.time_left > 0 && Input.is_action_just_pressed("ui_accept")):
		showText()

func displayText(new_textToDisplay, is_last_text = false):
	self.last_text = is_last_text
	self.textToDisplay = new_textToDisplay
	self.modulate = initial_modulate
	$Timer.start()

func showLetter():
	var nextLetter = textToDisplay.substr(self.text.length(), 1)
	self.text += str(nextLetter)
	
	if(textIsDisplayed()):
		$Timer.stop()
		if(!last_text):
			$AnimationPlayer.play("FadeOut")
		else:
			emit_signal("last_text_displayed")

func showText():
	self.text = self.textToDisplay;
	$Timer.stop()
	
	if(!last_text):
		$AnimationPlayer.play("LongFadeOut")
	else:
		emit_signal("last_text_displayed")

func textIsDisplayed():
	return self.text == textToDisplay

func fin():
	self.text = ""
	self.textToDisplay = ""
	emit_signal("text_displayed")
