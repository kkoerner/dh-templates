extends Node2D

func _process(_delta):
	if Input.is_action_just_pressed("ui_pause"):
		toggle_pause()

func toggle_pause():
	get_tree().paused = !get_tree().paused
