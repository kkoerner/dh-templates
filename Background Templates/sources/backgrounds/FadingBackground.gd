extends Node2D

export (Array, Texture) var textures
export (Array, float) var display_durations

export(Array, Vector2) var positions
export(Array, Vector2) var offsets
export(Array, float) var speeds
export(Array, bool) var moves_lefts
export(Array, int) var reset_points

var active_sprite
var current_bg = 0

func _ready():
	if(textures.size() > 1):
		$DisplayDuration.wait_time = display_durations[current_bg]
		$DisplayDuration.start()
	init_bg()

func init_bg():
	reset_active_sprite()
	#set_speed()
	#set_reset_point()
	#set_texture()
	#reset_active_sprite()

func _on_DisplayDuration_timeout():
	set_current_bg()
	#set_speed()
	#set_reset_point()
	#set_texture()
	play_fade_animation()
	start_display_timer()
	reset_active_sprite()

#func set_speed():
#	if(active_sprite == $IntialActiveBG):
#		$InitialInactiveBG.speed = speeds[current_bg]
#	else:
#		$IntialActiveBG.speed = speeds[current_bg]

#func set_moves_left():
#	$InitialInactiveBG.moves_left = moves_lefts[current_bg]
#	$IntialActiveBG.moves_left = moves_lefts[current_bg]

#func set_reset_point():
#	if(active_sprite == $IntialActiveBG):
#		$InitialInactiveBG.reset_point = reset_points[current_bg]
#	else:
#		$IntialActiveBG.reset_point = reset_points[current_bg]


func set_current_bg():
	if current_bg < (display_durations.size() - 1):
		current_bg += 1
	else:
		current_bg = 0

#func set_texture():
#	if(active_sprite == $IntialActiveBG):
#		$InitialInactiveBG.texture = textures[current_bg]
#	else:
#		$IntialActiveBG.texture = textures[current_bg]

func play_fade_animation():
	if(active_sprite == $IntialActiveBG):
		$AnimationPlayer.play("FadeActiveToInactive")
	else:
		$AnimationPlayer.play("FadeInactiveToActive")

func reset_active_sprite():
	if(active_sprite == $IntialActiveBG):
		active_sprite = $InitialInactiveBG
	else:
		active_sprite = $IntialActiveBG
	
	active_sprite.texture = textures[current_bg]
	active_sprite.reset_point = reset_points[current_bg]
	active_sprite.position = positions[current_bg]
	active_sprite.speed = speeds[current_bg]
	active_sprite.offset = offsets[current_bg]
	active_sprite.direction =  -1 if moves_lefts[current_bg] else 1 
	active_sprite.store_initial_pos()
	
	print("P: ", active_sprite.position)
	print("O: ", active_sprite.position)
	print("ML: ", active_sprite.position)
	print("P: ", active_sprite.position)

func start_display_timer():
	$DisplayDuration.wait_time = display_durations[current_bg]
	$DisplayDuration.start() 
