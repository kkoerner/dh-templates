extends Sprite

export(float) var speed
export(bool) var moves_left = true
export(int) var reset_point = 0

var direction
var initial_position

func _ready():
	direction = -1 if moves_left else 1
	initial_position = self.global_position

func store_initial_pos():
	initial_position = self.global_position

func _process(delta):
	self.global_position.x += direction * delta * speed
	if(reset_required()):
		self.global_position.x = initial_position.x
	

func reset_required():
	var reset_required = false
	if direction == -1:
		reset_required = self.global_position.x <= reset_point
	elif direction == 1:
		reset_required = self.global_position.x >= reset_point
	return reset_required
	


