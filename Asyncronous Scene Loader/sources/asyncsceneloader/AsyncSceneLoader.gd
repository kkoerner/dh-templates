extends Node2D

export(String) var resource_to_load
export(bool) var autostart = false
export(float) var start_delay = 1.0

export (NodePath) var mutex_loader_path

var mutex_loader

func _ready():
	mutex_loader = get_node(mutex_loader_path)
	
	if(autostart):
		$Autostart.wait_time = start_delay
		$Autostart.start()

func start_loading_scene(scene_path := ""):
	if(scene_path != ""):
		self.resource_to_load = scene_path
	
	if(self.resource_to_load == ""):
		printerr("No scene for loading declared.")
	else:
		mutex_loader.queue_resource(resource_to_load)

func perform_scene_change():
	if(!mutex_loader.is_ready(resource_to_load)):
		$RetryTimer.start()
		$Sprite.show()
	else:
		mutex_loader.switch_scene(resource_to_load)
	
