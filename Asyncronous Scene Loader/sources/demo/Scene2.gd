extends Node2D

export (NodePath) var scene3_loader
export (NodePath) var scene4_loader

var active_scene_loader

func load_scene(scene_id):
	if(scene_id == 3):
		active_scene_loader = get_node(scene3_loader)
	else:
		active_scene_loader = get_node(scene4_loader)
	
	active_scene_loader.start_loading_scene()
	$AnimationPlayer.play("fadeout")

func on_fadeout_end():
	active_scene_loader.perform_scene_change()
