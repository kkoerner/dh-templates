extends Node2D

signal on_scene_change

var fadein_ended = false
 
func _process(delta):
	if(fadein_ended and Input.is_action_just_pressed("ui_accept")):
		emit_signal("on_scene_change")

func on_fadein_ended():
	fadein_ended = true
