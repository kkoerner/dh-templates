extends Node2D

var queue 
var current_scene

func _ready():
	queue = preload("res://mutexloader/resource_queue.gd").new()
	queue.start()
	
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func queue_resource(resource):
	queue.queue_resource(resource)

func is_ready(resource):
	return queue.is_ready((resource))

func switch_scene(resource):
	current_scene.queue_free()
	current_scene = queue.get_resource(resource).instance()
	resource = null
	get_node("/root").add_child(current_scene)
